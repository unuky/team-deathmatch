#include <a_samp>
#include <streamer>
#include <dini>
#include <zcmd>

#undef MAX_PLAYERS
#define MAX_PLAYERS         50

#define MAX_EQUIPOS         3
#define EQUIPO_NINGUNO      0
#define EQUIPO_AMERICA      1
#define EQUIPO_EUROPA       2

#define COLOR_AMERICA		0xFF0000AA
#define COLOR_EUROPA		0x0095FFAA
#define COLOR_NEGRO        	0x000000AA
#define COLOR_INFO          0xFFEBBDFF
#define COLOR_ERROR         0xFF9500FF
#define COLOR_ADMIN         0x009DFFFF
#define COLOR_DESTACADO     0x99FF00FF

#define MAX_TERRITORIOS     5
#define OCEAN_FLATS         1
#define CRANBERRY_STATION   2
#define KINGS               3
#define SPLANADE_NORTH      4

// GivePlayerMoney
#if defined _ALS_GivePlayerMoney
	#undef GivePlayerMoney
#else
	#define _ALS_GivePlayerMoney
#endif
#define GivePlayerMoney GivePlayerMoneyEx
// GetPlayerScore
#if defined _ALS_GetPlayerScore
	#undef GetPlayerScore
#else
	#define _ALS_GetPlayerScore
#endif
#define GetPlayerScore GetPlayerScoreEx
// BanEx
#if defined _ALS_BanEx
	#undef BanEx
#else
	#define _ALS_BanEx
#endif
#define BanEx BanEx2
// END HOOKS

forward DataUserLoad(playerid);
forward DataUserSave(playerid);
forward DataUserClean(playerid);
forward SetPlayerTeamFromClass(playerid, classid);
forward CrearZona(ZonaID, Float:X, Float:Y, Float:Z, Float:MinX, Float:MinY, Float:MaxX, Float:MaxY);
forward CargarZonas();
forward CrearIconoDeZona(playerid, ZonaID, Float:X, Float:Y);
forward CargarIconosDeZonas(playerid);
forward ActivarCaptura(playerid, zonaid);
forward ParpadearZona(playerid, zonaid);
forward ZonaCapturada(playerid, zonaid);
forward SetZoneColor(playerid, zonaid);
forward MostrarZonas();
forward LoadVehicles();
forward CuentaAtrasCaptura(playerid, zonaid);
forward DataZoneLoad();
forward DataZoneSave();
forward GetPlayerScoreEx(playerid);
forward GivePlayerMoneyEx(playerid, money);
forward DataAmericaLoad();
forward DataAmericaSave();
forward DataEuropaLoad();
forward DataEuropaSave();
forward GivePlayerMoneyFromTeam(playerid, money, team);
forward GetPlayerTeamEx(playerid);
forward KickEx(playerid, razon[]);
forward Kickear(playerid);
forward BanEx2(playerid, razon[]);
forward Banear(playerid, razon[]);

enum DataUsers
{
	Dinero,
	Admin,
	Puntos,
	Score,
	Lider,
	bool:Baneado
}
enum DataUsersOnline
{
	Estado,
	/*
	    0 - Conectando...
		1 - Login
		2 - Registro
		3 - Logueado
	*/
	NameOnline[MAX_PLAYER_NAME],
	Equipo
}
enum ZonasEnum
{
	Checkpoint,
	GangZone,
	Dueno,
	bool:Ataque
}
enum EquiposEnum
{
	Dinero,
	Muerte,
	Invasion,
	Hospital
}

new DIR_USUARIOS[11]    = "/Usuarios/";
new PlayersData[MAX_PLAYERS][DataUsers];
new PlayersDataOnline[MAX_PLAYERS][DataUsersOnline];
new Contador[MAX_PLAYERS][MAX_TERRITORIOS];
new Territorio[MAX_TERRITORIOS][ZonasEnum];
new Text:ConteoText[MAX_PLAYERS];
new Conteo[MAX_PLAYERS];
new ConteoTimer[MAX_PLAYERS];
new ZONA_AMERICA;
new ZONA_EUROPA;
new Equipos[MAX_EQUIPOS][EquiposEnum];
new Text:TeamInfoText;
// CHEATS
new ANTI_DINERO     		= 1;
new ANTI_JETPACK            = 1;
new ANTI_SOBEIT             = 1;
new ANTI_FAKEKILL           = 1;
new LastDeath[MAX_PLAYERS], DeathSpam[MAX_PLAYERS char];

main()
{

}

public OnGameModeInit()
{
	SetGameModeText("TDM / DM ESP");
	DataZoneLoad();
	CargarZonas();
	LoadVehicles();
	DisableInteriorEnterExits();
	UsePlayerPedAnims();
	DataAmericaLoad();
	DataEuropaLoad();
	
	AddPlayerClass(287,-2414.2751,331.5806,34.9755,238.5533,0,0,0,0,0,0); // America ID-0
	AddPlayerClass(286,-2414.2751,331.5806,34.9755,238.5533,0,0,0,0,0,0); // America ID-1
	AddPlayerClass(267,-2414.2751,331.5806,34.9755,238.5533,0,0,0,0,0,0); // America ID-2
	AddPlayerClass(285,-2743.7427,-289.5517,6.9970,220.0900,0,0,0,0,0,0); // Europa ID-3
	AddPlayerClass(283,-2743.7427,-289.5517,6.9970,220.0900,0,0,0,0,0,0); // Europa ID-4
	AddPlayerClass(280,-2743.7427,-289.5517,6.9970,220.0900,0,0,0,0,0,0); // Europa ID-5
	
	Territorio[OCEAN_FLATS][Ataque] 			= false;
	Territorio[CRANBERRY_STATION][Ataque] 		= false;
	Territorio[KINGS][Ataque]                   = false;
	Territorio[SPLANADE_NORTH][Ataque]          = false;
	
	ZONA_AMERICA = GangZoneCreate(-2335.7119, 389.8506, -2514.3867, 246.7269);
	ZONA_EUROPA = GangZoneCreate(-2612.7749, -218.5552, -2815.0698, -414.5305);
	
	for(new playerid; playerid < MAX_PLAYERS; playerid ++){
	ConteoText[playerid] = TextDrawCreate(192.000000, 416.000000, "Textdraw de cuenta atr�s en una captura de territorio");
	TextDrawBackgroundColor(ConteoText[playerid], 255);
	TextDrawFont(ConteoText[playerid], 1);
	TextDrawLetterSize(ConteoText[playerid], 0.460000, 1.900000);
	TextDrawColor(ConteoText[playerid], -1);
	TextDrawSetOutline(ConteoText[playerid], 1);
	TextDrawSetProportional(ConteoText[playerid], 1);   }
	
	TeamInfoText = TextDrawCreate(20.000000, 169.000000, "Paga por muerte: ~g~$150~n~~n~~w~Paga por invasion: ~g~$75~n~~n~~w~Coste del hospital: ~g~$200");
	TextDrawBackgroundColor(TeamInfoText, 255);
	TextDrawFont(TeamInfoText, 1);
	TextDrawLetterSize(TeamInfoText, 0.410000, 1.700000);
	TextDrawColor(TeamInfoText, -1);
	TextDrawSetOutline(TeamInfoText, 0);
	TextDrawSetProportional(TeamInfoText, 1);
	TextDrawSetShadow(TeamInfoText, 1);
	TextDrawUseBox(TeamInfoText, 1);
	TextDrawBoxColor(TeamInfoText, 255);
	TextDrawTextSize(TeamInfoText, 146.000000, 0.000000);
	return 1;
}

public OnGameModeExit()
{
	DataZoneSave();
	DataAmericaSave();
	DataEuropaSave();
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	SetPlayerPos(playerid, -2379.8848,-2805.7505,5.4453);
	SetPlayerFacingAngle(playerid, 64.9468);
	SetPlayerCameraPos(playerid, -2382.7786, -2805.5007, 5.1064);
	SetPlayerCameraLookAt(playerid, -2364.0488, -2798.7034, 9.4741);
	SetPlayerTeamFromClass(playerid, classid);
	
	switch (classid)
	{
	    case 0..2:
	    {
	        GameTextForPlayer(playerid, "~r~America", 2000, 6);
	        new string[100];
	        format(string, sizeof(string), "Paga por muerte: ~g~$%i~n~~n~~w~Paga por invasion: ~g~$%i~n~~n~~w~Coste del hospital: ~g~$%i",
	        Equipos[EQUIPO_AMERICA][Muerte],
	        Equipos[EQUIPO_AMERICA][Invasion],
	        Equipos[EQUIPO_AMERICA][Hospital]);
	        TextDrawSetString(TeamInfoText, string);
	        TextDrawShowForPlayer(playerid, TeamInfoText);
		}
		case 3..5:
		{
		    GameTextForPlayer(playerid, "~b~Europa", 2000, 6);
	        new string[100];
	        format(string, sizeof(string), "Paga por muerte: ~g~$%i~n~~n~~w~Paga por invasion: ~g~$%i~n~~n~~w~Coste del hospital: ~g~$%i",
	        Equipos[EQUIPO_EUROPA][Muerte],
	        Equipos[EQUIPO_EUROPA][Invasion],
	        Equipos[EQUIPO_EUROPA][Hospital]);
	        TextDrawSetString(TeamInfoText, string);
	        TextDrawShowForPlayer(playerid, TeamInfoText);
		}
	}
	return 1;
}
public OnPlayerConnect(playerid)
{
	PlayersDataOnline[playerid][Estado] = 0;
	CargarIconosDeZonas(playerid);
	MostrarZonas();
	TextDrawHideForPlayer(playerid, ConteoText[playerid]);
    GetPlayerName(playerid, PlayersDataOnline[playerid][NameOnline], MAX_PLAYER_NAME);
    GangZoneShowForAll(ZONA_AMERICA, COLOR_AMERICA);
    GangZoneShowForAll(ZONA_EUROPA, COLOR_EUROPA);
    
	new File[39];
	format(File, sizeof(File), "%s%s.ini", DIR_USUARIOS, PlayersDataOnline[playerid][NameOnline]);
	
	if ( dini_Exists(File) )
	{
	    ShowPlayerDialog(playerid, 0, DIALOG_STYLE_PASSWORD, "Iniciar sesi�n", "{FFFFFF}Bienvenido a {99FF00}Shillons! {FFFFFF}Ingrese su \ncontrase�a para iniciar sesi�n.", "Conectar", "Cancelar");
	    PlayersDataOnline[playerid][Estado] = 1;
	}
	else
	{
	    ShowPlayerDialog(playerid, 1, DIALOG_STYLE_PASSWORD, "Registrar cuenta", "{FFFFFF}Bienvenido a {99FF00}Shillons! {FFFFFF}Ingrese \nuna contrase�a para crear una cuenta.", "Registrar", "Cancelar");
	    PlayersDataOnline[playerid][Estado] = 2;
	}
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
    DataUserSave(playerid);
	return 1;
}

public OnPlayerUpdate(playerid)
{
	if (ANTI_DINERO == 1)
	{
		if (PlayersData[playerid][Dinero] != GetPlayerMoney(playerid))
		{
			ResetPlayerMoney(playerid);
			GivePlayerMoney(playerid, PlayersData[playerid][Dinero]);
		}
	}
	if (ANTI_JETPACK == 1)
	{
	    new anim = GetPlayerSpecialAction(playerid);
	    if (anim == SPECIAL_ACTION_USEJETPACK)
	    {
	        BanEx(playerid, "Cheat de jetpack");
		}
	}
	if (ANTI_SOBEIT == 1)
	{
	    new Float:pos[3];
	    GetPlayerCameraFrontVector(playerid, pos[0], pos[1], pos[2]);
	    if (pos[2] < -0.8)
	    {
	        BanEx(playerid, "Uso de sobeit");
		}
	}
	return 1;
}

public OnPlayerSpawn(playerid)
{
	TextDrawHideForPlayer(playerid, TeamInfoText);
	GivePlayerWeapon(playerid, 24, 99999);
	GivePlayerWeapon(playerid, 25, 99999);
	GivePlayerWeapon(playerid, 29, 99999);
	GivePlayerWeapon(playerid, 31, 99999);
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	if (PlayersDataOnline[killerid][Equipo] == PlayersDataOnline[playerid][Equipo])
	{
	    SetPlayerHealth(killerid, 0.0);
	    GameTextForPlayer(killerid, "~w~No mates a tu compa�ero ~r~(muerto)", 3000, 3);
	}
	else
	{
	    if (killerid != INVALID_PLAYER_ID)
	    {
	        GivePlayerMoneyFromTeam(killerid, Equipos[GetPlayerTeamEx(killerid)][Muerte], GetPlayerTeamEx(killerid));
	        GivePlayerMoneyFromTeam(playerid, Equipos[GetPlayerTeamEx(playerid)][Hospital], GetPlayerTeamEx(playerid));
	        PlayersData[killerid][Puntos] += 2;
	        SetPlayerScore(playerid, GetPlayerScore(playerid));
	        SendClientMessage(killerid, COLOR_INFO, "+2 PUNTOS");
		}
	}
	if (ANTI_FAKEKILL == 1) // Forum.SA-MP.com
	{
		new time = gettime();
		switch(time - LastDeath[playerid])
		{
			case 0 .. 3:
	 		{
				DeathSpam{playerid}++;
				if(DeathSpam{playerid} == 3)
				{
				    BanEx(playerid, "Cheat de fake kill");
				}
	  		}
	   		default: DeathSpam{playerid} = 0;
		}
		LastDeath[playerid] = time;
	}
	return 1;
}

// START COMMANDS
CMD:dinero(playerid, params[])
{
	if (PlayersData[playerid][Admin] >= 2)
	{
		if (!isnull(params))
		{
		    if ( IsPlayerConnected( strval(params[0]) ) )
		    {
		    	new string[79];
				GivePlayerMoney(strval(params[0]), strval(params[1]));
				format(string, sizeof(string), "Administraci�n: Le has dado {99FF00}$%i {009DFF}a el usuario {99FF00}%s.", strval(params[1]), PlayersDataOnline[strval(params[0])][NameOnline]);
				SendClientMessage(playerid, COLOR_ADMIN, string);
				format(string, sizeof(string), "Administraci�n: El admin {99FF00}%s {009DFF}te ha dado {99FF00}$%i.", PlayersDataOnline[playerid][NameOnline], strval(params[1]));
				SendClientMessage(playerid, COLOR_ADMIN, string);
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "Error: El usuario al que le desea aplicar el comando no est� conectado.");
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /dinero [ID] [Cantidad].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:admin(playerid, params[])
{
	new toplayer = strval(params[0]);
	if (PlayersData[playerid][Admin] >= 2)
	{
	    if (!isnull(params))
	    {
	        if (IsPlayerConnected(toplayer))
	        {
	            new nivel = strval(params[1]), string[72];
	            PlayersData[toplayer][Admin] = nivel;
	            format(string, sizeof(string), "Administraci�n: Le has dado a {99FF00}%s {009DFF}admin nivel {99FF00}%i.", PlayersDataOnline[toplayer][NameOnline], nivel);
	            SendClientMessage(playerid, COLOR_ADMIN, string);
	            format(string, sizeof(string), "Administraci�n: Ahora eres administrador nivel {99FF00}%i!", nivel);
	            SendClientMessage(toplayer, COLOR_ADMIN, string);
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "Error: El usuario al que le desea aplicar el comando no est� conectado.");
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /admin [ID] [Nivel].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:v(playerid, params[])
{
	new modelo = strval(params[0]), string[70];
	if (PlayersData[playerid][Admin] >= 2)
	{
	    if (!isnull(params))
	    {
	    	if (modelo >= 400 && modelo <= 611)
	    	{
				new Float:x, Float:y, Float:z;
				GetPlayerPos(playerid, x, y, z);
				CreateVehicle(modelo, x, y, z, 0, 0, 0, -1);
				format(string, sizeof(string), "Administraci�n: Has hecho aparecer el veh�culo con el modelo ID {99FF00}%i.", modelo);
				SendClientMessage(playerid, COLOR_ADMIN, string);
			}
			else
			{
		    	SendClientMessage(playerid, COLOR_ERROR, "Error: El modelo introducido no es v�lido, escoja un modelo del 400 al 611.");
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /v [Modelo].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:estadisticas(playerid, params[])
{
	new string[50];
	SendClientMessage(playerid, COLOR_INFO, "Estad�sticas de tu cuenta:");
	format(string, sizeof(string), "Dinero: $%i - Puntos: %i - Score: %i", PlayersData[playerid][Dinero], PlayersData[playerid][Puntos], GetPlayerScore(playerid));
	SendClientMessage(playerid, COLOR_INFO, string);
	return 1;
}
CMD:stats(playerid, params[])
{
	return cmd_estadisticas(playerid, params);
}
CMD:equipo(playerid, params[])
{
	new string[87], jugador = strval(params[0]), equipo = strval(params[1]);
	if (PlayersData[playerid][Admin] >= 2)
	{
	    if (!isnull(params))
	    {
	        if (IsPlayerConnected(jugador))
	        {
				PlayersData[jugador][Lider] = equipo;
				format(string, sizeof(string), "Administraci�n: Le has puesto l�der del equipo {99FF00}%i {009DFF}al usuario {99FF00}%s.", equipo, PlayersDataOnline[jugador][NameOnline]);
				SendClientMessage(playerid, COLOR_ADMIN, string);
				format(string, sizeof(string), "Administraci�n: El admin {99FF00}%s {009DFF}te ha hecho l�der del equipo {99FF00}%i.", PlayersDataOnline[playerid][NameOnline], equipo);
				SendClientMessage(jugador, COLOR_ADMIN, string);
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "Error: El usuario al que le desea aplicar el comando no est� conectado.");
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /equipo [ID] [Equipo].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:deposito(playerid, params[])
{
	if (PlayersData[playerid][Lider] != 0)
	{
	    new dinero = Equipos[PlayersData[playerid][Lider]][Dinero];
	    new string[43];
	    format(string, sizeof(string), "El dep�sito de tu equipo tiene {99FF00}$%i.", dinero);
	    SendClientMessage(playerid, COLOR_INFO, string);
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:pagapormuerte(playerid, params[])
{
	if (PlayersData[playerid][Lider] != 0)
	{
	    if (!isnull(params))
	    {
	    	new paga = strval(params[0]);
	    	new equipo = PlayersData[playerid][Lider];
	    	Equipos[equipo][Muerte] = paga;
	    	new string[50];
	    	format(string, sizeof(string), "Ahora pagas {99FF00}$%i {FFEBBD}por cada muerte realizada.", paga);
			SendClientMessage(playerid, COLOR_INFO, string);
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /pagapormuerte [Cantidad].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:pagaporinvasion(playerid, params[])
{
	if (PlayersData[playerid][Lider] != 0)
	{
	    if (!isnull(params))
	    {
	    	new string[52], paga = strval(params[0]);
	    	new equipo = PlayersData[playerid][Lider];
	    	Equipos[equipo][Invasion] = paga;
	    	format(string, sizeof(string), "Ahora pagas {99FF00}$%i {FFEBBD}por cada invasi�n realizada.", paga);
	    	SendClientMessage(playerid, COLOR_INFO, string);
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /pagaporinvasion [Cantidad].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:costehospital(playerid, params[])
{
	if (PlayersData[playerid][Lider] != 0)
	{
	    if (!isnull(params))
	    {
	    	new string[54], coste = strval(params[0]);
	    	new equipo = PlayersData[playerid][Lider];
	    	Equipos[equipo][Hospital] = coste;
	    	format(string, sizeof(string), "Ahora cobras {99FF00}$%i {FFEBBD}por cada ingreso al hospital.", coste);
	    	SendClientMessage(playerid, -1, string);
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /costehospital [Cantidad].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:kick(playerid, params[])
{
	if (PlayersData[playerid][Admin] == 1)
	{
	    if (!isnull(params))
	    {
	        if (IsPlayerConnected(strval(params[0])))
	        {
	    		new string[162];
				KickEx(strval(params[0]), params[1]);
				format(string, sizeof(string), "Administraci�n: El usuario {99FF00}%s {009DFF}ha sido kickeado por {99FF00}%s. {009DFF}Raz�n: {99FF00}%s", PlayersDataOnline[strval(params[0])][NameOnline], PlayersDataOnline[playerid][NameOnline], params[1]);
				SendClientMessageToAll(COLOR_ADMIN, string);
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "Error: El usuario al que le desea aplicar el comando no est� conectado.");
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /kick [ID] [Raz�n].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:ban(playerid, params[])
{
	if (PlayersData[playerid][Admin] == 1)
	{
	    if (!isnull(params))
	    {
			if (IsPlayerConnected(strval(params[0])))
			{
	    		new string[162];
				BanEx(strval(params[0]), params[1]);
				format(string, sizeof(string), "Administraci�n: El usuario {99FF00}%s {009DFF}ha sido baneado por {99FF00}%s. {009DFF}Raz�n: {99FF00}%s", PlayersDataOnline[strval(params[0])][NameOnline], PlayersDataOnline[playerid][NameOnline], params[1]);
				SendClientMessageToAll(COLOR_ADMIN, string);
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "Error: El usuario al que le desea aplicar el comando no est� conectado.");
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_ERROR, "Error: No has escrito los par�metros correctos. Use /ban [ID] [Raz�n].");
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_ERROR, "Error: No puedes usar este comando.");
	}
}
CMD:mirar(playerid, params[])
{
	new toplayer = strval(params[0]);
	if (PlayersData[playerid][Admin] >= 1)
	{
		if (strlen(params[0]) < 1)
		{
		    TogglePlayerSpectating(playerid, 0);
		}
		else if (IsPlayerConnected(toplayer))
		{
		    TogglePlayerSpectating(playerid, 1);
		    PlayerSpectatePlayer(playerid, toplayer);
		}
	}
}
// END COMMANDS

public OnPlayerCommandReceived(playerid, cmdtext[])
{
    printf("%s[%i] || %s", PlayersDataOnline[playerid][NameOnline], playerid, cmdtext);
    return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	switch ( dialogid )
	{
	    // LOGIN
	    case 0:
	    {
			new File[39], Comprobar[128];
			if ( !strlen(inputtext) )
			{
			    ShowPlayerDialog(playerid, 0, DIALOG_STYLE_PASSWORD, "Iniciar sesi�n", "{FFFFFF}Bienvenido a {99FF00}Shillons! {FFFFFF}Ingrese su \ncontrase�a para iniciar sesi�n.", "Conectar", "Cancelar");
			}
			if ( response == 0 )
			{
			    Kick(playerid);
			}

			format(File, sizeof(File), "%s%s.ini", DIR_USUARIOS, PlayersDataOnline[playerid][NameOnline]);
			format(Comprobar, sizeof(Comprobar), "%s", dini_Get(File, "Contrase�a"));
			
			if ( !strcmp (inputtext, Comprobar) )
			{
				DataUserLoad(playerid);
				PlayersDataOnline[playerid][Estado] = 3;
				if (PlayersData[playerid][Baneado] == true)
				{
				    KickEx(playerid, "Tu cuenta est� baneada.");
				}
			}
			else
			{
				ShowPlayerDialog(playerid, 2, DIALOG_STYLE_MSGBOX, "Contrase�a erronea", "{FFFFFF}No has escrito la contrase�a correcta de esta cuenta.", "Aceptar", "");
				Kick(playerid);
			}
		}
		// REGISTRO
		case 1:
		{
		    new File[39], string[201];
			if ( !strlen(inputtext) )
			{
				ShowPlayerDialog(playerid, 1, DIALOG_STYLE_INPUT, "Registrar cuenta", "{FFFFFF}Bienvenido a {99FF00}Shillons! {FFFFFF}Ingrese \nuna contrase�a para crear una cuenta.", "Registrar", "Cancelar");
			}
			if ( response == 0 )
			{
			    Kick(playerid);
			}
			format(File, sizeof(File), "%s%s.ini", DIR_USUARIOS, PlayersDataOnline[playerid][NameOnline]);
			dini_Create(File);
			dini_Set(File, "Contrase�a", inputtext);
			DataUserClean(playerid);
			format(string,sizeof(string),"{FFFFFF}Ha elegido la contrase�a: {0080FF}%s \n{FFFFFF}Ingresela de nuevo por favor.", inputtext);
			ShowPlayerDialog(playerid, 0, DIALOG_STYLE_PASSWORD, "Registrar cuenta", string, "Aceptar", "Cancelar");
		}
	}
	return 1;
}

public DataUserLoad(playerid)
{
	new File[39];
	format(File, sizeof(File), "%s%s.ini", DIR_USUARIOS, PlayersDataOnline[playerid][NameOnline]);
	
	ResetPlayerMoney(playerid);
	PlayersData[playerid][Dinero] = dini_Int(File, "Dinero");
	GivePlayerMoney(playerid, PlayersData[playerid][Dinero]);
	PlayersData[playerid][Admin] = dini_Int(File, "Admin");
	PlayersData[playerid][Puntos] = dini_Int(File, "Puntos");
	SetPlayerScore(playerid, dini_Int(File, "Score"));
	PlayersData[playerid][Lider] = dini_Int(File, "Lider");
	PlayersData[playerid][Baneado] = bool:dini_Bool(File, "Baneado");
	return 1;
}

public DataUserSave(playerid)
{
	if (PlayersDataOnline[playerid][Estado] == 3)
	{
		new File[39];
		format(File, sizeof(File), "%s%s.ini", DIR_USUARIOS, PlayersDataOnline[playerid][NameOnline]);
		
		dini_IntSet(File, "Dinero", PlayersData[playerid][Dinero]);
		dini_IntSet(File, "Admin", PlayersData[playerid][Admin]);
		dini_IntSet(File, "Puntos", PlayersData[playerid][Puntos]);
		dini_IntSet(File, "Score", GetPlayerScore(playerid));
		dini_IntSet(File, "Lider", PlayersData[playerid][Lider]);
		dini_BoolSet(File, "Baneado", PlayersData[playerid][Baneado]);
		return 1;
	}
	return 1;
}

public DataUserClean(playerid)
{
	if ( IsPlayerConnected(playerid) )
	{
		new File[39];
		format(File, sizeof(File), "%s%s.ini", DIR_USUARIOS, PlayersDataOnline[playerid][NameOnline]);
		
		dini_IntSet(File, "Dinero", 0);
		dini_IntSet(File, "Admin", 0);
		dini_IntSet(File, "Puntos", 0);
		dini_IntSet(File, "Score", 0);
		dini_IntSet(File, "Lider", 0);
		dini_BoolSet(File, "Baneado", false);
		return 1;
	}
	return 1;
}

public SetPlayerTeamFromClass(playerid, classid)
{
	switch(classid)
	{
	    case 0..2:
	    {
	        PlayersDataOnline[playerid][Equipo] = EQUIPO_AMERICA;
	        SetPlayerColor(playerid, COLOR_AMERICA);
		}
		case 3..5:
		{
		    PlayersDataOnline[playerid][Equipo] = EQUIPO_EUROPA;
		    SetPlayerColor(playerid, COLOR_EUROPA);
		}
	}
}

public CrearZona(ZonaID, Float:X, Float:Y, Float:Z, Float:MinX, Float:MinY, Float:MaxX, Float:MaxY)
{
    Territorio[ZonaID][Checkpoint] = CreateDynamicCP(X, Y, Z, 3.0, -1, -1, -1, 100.0);
    Territorio[ZonaID][GangZone] = GangZoneCreate(MinX, MinY, MaxX, MaxY);
}

public CargarZonas()
{
	CrearZona(OCEAN_FLATS, -2754.4829, -149.8527, 6.9267, -2684.5679, -63.3597, -2814.3396, -210.1641);
	CrearZona(CRANBERRY_STATION, -1968.6595, 162.1815, 27.6875, -2017.1252, 218.1302, -1932.3351, 70.2552);
	CrearZona(KINGS, -2227.9053, 252.0235, 35.3203, -2255.6416, 212.0317, -2143.8879, 314.3695);
	CrearZona(SPLANADE_NORTH, -2079.1914, 1422.3856, 7.1007, -2061.6038, 1436.4038, -2118.8999, 1348.8927);
}

public CrearIconoDeZona(playerid, ZonaID, Float:X, Float:Y)
{
	SetPlayerMapIcon(playerid, ZonaID + 50, X, Y, 0, 56, -1, MAPICON_GLOBAL);
}

public CargarIconosDeZonas(playerid)
{
	CrearIconoDeZona(playerid, OCEAN_FLATS, -2754.4829, -149.8527);
	CrearIconoDeZona(playerid, CRANBERRY_STATION, -1968.6595, 162.1815);
	CrearIconoDeZona(playerid, KINGS, -2227.9053, 252.0235);
	CrearIconoDeZona(playerid, SPLANADE_NORTH, -2079.1914, 1422.3856);
}

public OnPlayerEnterDynamicCP(playerid, checkpointid)
{
	switch(checkpointid)
	{
	    case OCEAN_FLATS:
	    {
	        if (Territorio[OCEAN_FLATS][Ataque] == false)
	        {
	            if (Territorio[OCEAN_FLATS][Dueno] != PlayersDataOnline[playerid][Equipo])
	            {
	                ActivarCaptura(playerid, OCEAN_FLATS);
				}
			}
		}
		case CRANBERRY_STATION:
		{
		    if (Territorio[CRANBERRY_STATION][Ataque] == false)
	        {
	            if (Territorio[CRANBERRY_STATION][Dueno] != PlayersDataOnline[playerid][Equipo])
	            {
	                ActivarCaptura(playerid, CRANBERRY_STATION);
				}
			}
		}
		case KINGS:
		{
		    if (Territorio[KINGS][Ataque] == false)
	        {
	            if (Territorio[KINGS][Dueno] != PlayersDataOnline[playerid][Equipo])
	            {
	                ActivarCaptura(playerid, KINGS);
				}
			}
		}
		case SPLANADE_NORTH:
		{
		    if (Territorio[SPLANADE_NORTH][Ataque] == false)
	        {
	            if (Territorio[SPLANADE_NORTH][Dueno] != PlayersDataOnline[playerid][Equipo])
	            {
	                ActivarCaptura(playerid, SPLANADE_NORTH);
				}
			}
		}
	}
}

public OnPlayerLeaveDynamicCP(playerid, checkpointid)
{
	switch(checkpointid)
	{
	    case OCEAN_FLATS:
	    {
	        if (Territorio[OCEAN_FLATS][Ataque] == true)
	        {
	        	Territorio[OCEAN_FLATS][Ataque] = false;
				KillTimer(Contador[playerid][OCEAN_FLATS]);
				GangZoneStopFlashForAll(Territorio[OCEAN_FLATS][GangZone]);
				KillTimer(ConteoTimer[playerid]);
				TextDrawHideForPlayer(playerid, ConteoText[playerid]);
				GameTextForPlayer(playerid, "~r~Invasion anulada", 3000, 3);
			}
		}
		case CRANBERRY_STATION:
		{
		    if (Territorio[CRANBERRY_STATION][Ataque] == true)
		    {
				Territorio[CRANBERRY_STATION][Ataque] = false;
				KillTimer(Contador[playerid][CRANBERRY_STATION]);
				GangZoneStopFlashForAll(Territorio[CRANBERRY_STATION][GangZone]);
				KillTimer(ConteoTimer[playerid]);
				TextDrawHideForPlayer(playerid, ConteoText[playerid]);
				GameTextForPlayer(playerid, "~r~Invasion anulada", 3000, 3);
			}
		}
		case KINGS:
		{
		    if (Territorio[KINGS][Ataque] == true)
		    {
				Territorio[KINGS][Ataque] = false;
				KillTimer(Contador[playerid][KINGS]);
				GangZoneStopFlashForAll(Territorio[KINGS][GangZone]);
				KillTimer(ConteoTimer[playerid]);
				TextDrawHideForPlayer(playerid, ConteoText[playerid]);
				GameTextForPlayer(playerid, "~r~Invasion anulada", 3000, 3);
			}
		}
		case SPLANADE_NORTH:
		{
		    if (Territorio[SPLANADE_NORTH][Ataque] == true)
		    {
				Territorio[SPLANADE_NORTH][Ataque] = false;
				KillTimer(Contador[playerid][SPLANADE_NORTH]);
				GangZoneStopFlashForAll(Territorio[SPLANADE_NORTH][GangZone]);
				KillTimer(ConteoTimer[playerid]);
				TextDrawHideForPlayer(playerid, ConteoText[playerid]);
				GameTextForPlayer(playerid, "~r~Invasion anulada", 3000, 3);
			}
		}
	}
}

public ActivarCaptura(playerid, zonaid)
{
	if (!IsPlayerInAnyVehicle(playerid))
	{
	    Territorio[zonaid][Ataque] = true;
	    Contador[playerid][zonaid] = SetTimerEx("ZonaCapturada", 30000, false, "ii", playerid, zonaid);
	    ParpadearZona(playerid, zonaid);
	    ConteoTimer[playerid] = SetTimerEx("CuentaAtrasCaptura", 1000, true, "ii", playerid, zonaid);
	    Conteo[playerid] = 30;
	}
}

public ParpadearZona(playerid, zonaid)
{
	if (PlayersDataOnline[playerid][Equipo] == EQUIPO_AMERICA)
	{
	    GangZoneFlashForAll(Territorio[zonaid][GangZone], COLOR_AMERICA);
	}
	else if (PlayersDataOnline[playerid][Equipo] == EQUIPO_EUROPA)
	{
	    GangZoneFlashForAll(Territorio[zonaid][GangZone], COLOR_EUROPA);
	}
}

public ZonaCapturada(playerid, zonaid)
{
	Territorio[zonaid][Ataque] = false;
	KillTimer(Contador[playerid][zonaid]);
	Territorio[zonaid][Dueno] = PlayersDataOnline[playerid][Equipo];
	GangZoneStopFlashForAll(Territorio[zonaid][GangZone]);
	SetZoneColor(playerid, zonaid);
	KillTimer(ConteoTimer[playerid]);
	TextDrawHideForPlayer(playerid, ConteoText[playerid]);
	PlayersData[playerid][Puntos]++;
	SetPlayerScore(playerid, GetPlayerScore(playerid));
	GivePlayerMoneyFromTeam(playerid, Equipos[GetPlayerTeamEx(playerid)][Invasion], GetPlayerTeamEx(playerid));
	SendClientMessage(playerid, COLOR_INFO, "+1 PUNTO");
	GameTextForPlayer(playerid, "~g~Territorio invadido", 3000, 3);
}

public SetZoneColor(playerid, zonaid)
{
	if (PlayersDataOnline[playerid][Equipo] == EQUIPO_AMERICA)
	{
	    GangZoneShowForAll(Territorio[zonaid][GangZone], COLOR_AMERICA);
	}
	else if (PlayersDataOnline[playerid][Equipo] == EQUIPO_EUROPA)
	{
	    GangZoneShowForAll(Territorio[zonaid][GangZone], COLOR_EUROPA);
	}
}

public MostrarZonas()
{
	// OCEAN_FLATS
	if (Territorio[OCEAN_FLATS][Dueno] == EQUIPO_NINGUNO)
	{
	    GangZoneShowForAll(Territorio[OCEAN_FLATS][GangZone], COLOR_NEGRO);
	}
	else if (Territorio[OCEAN_FLATS][Dueno] == EQUIPO_AMERICA)
	{
	    GangZoneShowForAll(Territorio[OCEAN_FLATS][GangZone], COLOR_AMERICA);
	}
	else if (Territorio[OCEAN_FLATS][Dueno] == EQUIPO_EUROPA)
	{
	    GangZoneShowForAll(Territorio[OCEAN_FLATS][GangZone], COLOR_EUROPA);
	}
	// CRANBERRY_STATION
	if (Territorio[CRANBERRY_STATION][Dueno] == EQUIPO_NINGUNO)
	{
	    GangZoneShowForAll(Territorio[CRANBERRY_STATION][GangZone], COLOR_NEGRO);
	}
	else if (Territorio[CRANBERRY_STATION][Dueno] == EQUIPO_AMERICA)
	{
	    GangZoneShowForAll(Territorio[CRANBERRY_STATION][GangZone], COLOR_AMERICA);
	}
	else if (Territorio[CRANBERRY_STATION][Dueno] == EQUIPO_EUROPA)
	{
	    GangZoneShowForAll(Territorio[CRANBERRY_STATION][GangZone], COLOR_EUROPA);
	}
	// KINGS
	if (Territorio[KINGS][Dueno] == EQUIPO_NINGUNO)
	{
	    GangZoneShowForAll(Territorio[KINGS][GangZone], COLOR_NEGRO);
	}
	else if (Territorio[KINGS][Dueno] == EQUIPO_AMERICA)
	{
	    GangZoneShowForAll(Territorio[KINGS][GangZone], COLOR_AMERICA);
	}
	else if (Territorio[KINGS][Dueno] == EQUIPO_EUROPA)
	{
	    GangZoneShowForAll(Territorio[KINGS][GangZone], COLOR_EUROPA);
	}
	// SPLANADE_NORTH
	if (Territorio[SPLANADE_NORTH][Dueno] == EQUIPO_NINGUNO)
	{
	    GangZoneShowForAll(Territorio[SPLANADE_NORTH][GangZone], COLOR_NEGRO);
	}
	else if (Territorio[SPLANADE_NORTH][Dueno] == EQUIPO_AMERICA)
	{
	    GangZoneShowForAll(Territorio[SPLANADE_NORTH][GangZone], COLOR_AMERICA);
	}
	else if (Territorio[SPLANADE_NORTH][Dueno] == EQUIPO_EUROPA)
	{
	    GangZoneShowForAll(Territorio[SPLANADE_NORTH][GangZone], COLOR_EUROPA);
	}
}

public LoadVehicles()
{
	// Equipo Europa
	AddStaticVehicleEx(470, -2741.7949, -281.8259, 6.7692, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2756.7268, -281.9937, 6.7692, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2786.4092, -282.1835, 6.7692, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2762.4138, -295.6024, 6.7692, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2771.0825, -312.1686, 6.7692, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(490, -2774.0137, -282.0363, 7.2187, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(490, -2763.3633, -312.5589, 7.2187, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(431, -2743.6467, -303.5061, 6.9777, 319.5638, -1, -1, 900000);
	AddStaticVehicleEx(427, -2772.5203, -295.5550, 7.0409, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(427, -2751.9373, -282.1983, 7.0409, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(468, -2766.8018, -281.1929, 6.6343, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(468, -2757.7637, -295.1725, 6.6343, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(468, -2767.5173, -295.1457, 6.6343, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(468, -2768.5198, -312.7105, 6.6343, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(468, -2774.0059, -312.7756, 6.6343, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(468, -2737.2915, -281.7974, 6.6343, 0.0000, -1, -1, 900000);

	// Equipo Am�rica
	AddStaticVehicleEx(470, -2425.0864, 318.0247, 34.7830, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2426.5911, 312.8973, 34.7830, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2409.4622, 345.8685, 34.8237, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(470, -2406.3713, 349.2251, 34.8237, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(490, -2435.3064, 281.3873, 35.4096, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(490, -2439.6982, 281.6613, 35.4096, 0.0000, -1, -1, 900000);
	AddStaticVehicleEx(433, -2437.1731, 293.2602, 35.7122, 295.3692, -1, -1, 900000);
	AddStaticVehicleEx(433, -2437.2825, 299.5917, 35.7122, 295.3692, -1, -1, 900000);
	AddStaticVehicleEx(468, -2416.4619, 308.5665, 34.7899, 292.0657, -1, -1, 900000);
	AddStaticVehicleEx(468, -2416.0161, 310.3598, 34.7899, 292.0657, -1, -1, 900000);
	AddStaticVehicleEx(468, -2415.2615, 312.2568, 34.7899, 292.0657, -1, -1, 900000);
	AddStaticVehicleEx(468, -2414.6367, 313.8614, 34.7899, 292.0657, -1, -1, 900000);
	AddStaticVehicleEx(468, -2413.9067, 315.5066, 34.7899, 292.0657, -1, -1, 900000);
}

public CuentaAtrasCaptura(playerid, zonaid)
{
	if (IsPlayerInDynamicCP(playerid, zonaid) && Territorio[zonaid][Ataque] == true)
	{
	    new string[64];
	    Conteo[playerid]--;
	    format(string, sizeof(string), "~w~Quedan ~r~%i ~y~segundos ~w~para ~g~invadir el territorio", Conteo[playerid]);
	    TextDrawShowForPlayer(playerid, ConteoText[playerid]);
	    TextDrawSetString(ConteoText[playerid], string);
	}
}

public DataZoneLoad()
{
	new File[31] = "Duenos de los territorios.ini";
    Territorio[OCEAN_FLATS][Dueno] = dini_Int(File, "OCEAN_FLATS");
    Territorio[CRANBERRY_STATION][Dueno] = dini_Int(File, "CRANBERRY_STATION");
    Territorio[KINGS][Dueno] = dini_Int(File, "KINGS");
    Territorio[SPLANADE_NORTH][Dueno] = dini_Int(File, "SPLANADE_NORTH");
}

public DataZoneSave()
{
	new File[31] = "Duenos de los territorios.ini";
	dini_IntSet(File, "OCEAN_FLATS", Territorio[OCEAN_FLATS][Dueno]);
	dini_IntSet(File, "CRANBERRY_STATION", Territorio[CRANBERRY_STATION][Dueno]);
	dini_IntSet(File, "KINGS", Territorio[KINGS][Dueno]);
	dini_IntSet(File, "SPLANADE_NORTH", Territorio[SPLANADE_NORTH][Dueno]);
}

public GetPlayerScoreEx(playerid)
{
	return floatround(PlayersData[playerid][Puntos]/10);
}

public GivePlayerMoneyEx(playerid, money)
{
	PlayersData[playerid][Dinero] = PlayersData[playerid][Dinero] + money;
	GivePlayerMoney(playerid, money);
}

public DataAmericaLoad()
{
	new File[22] = "Datos de America.ini";
	Equipos[EQUIPO_AMERICA][Dinero] = dini_Int(File, "Dinero");
	Equipos[EQUIPO_AMERICA][Muerte] = dini_Int(File, "Muerte");
	Equipos[EQUIPO_AMERICA][Invasion] = dini_Int(File, "Invasion");
	Equipos[EQUIPO_AMERICA][Hospital] = dini_Int(File, "Hospital");
}

public DataAmericaSave()
{
    new File[22] = "Datos de America.ini";
    dini_IntSet(File, "Dinero", Equipos[EQUIPO_AMERICA][Dinero]);
    dini_IntSet(File, "Muerte", Equipos[EQUIPO_AMERICA][Muerte]);
    dini_IntSet(File, "Invasion", Equipos[EQUIPO_AMERICA][Invasion]);
    dini_IntSet(File, "Hospital", Equipos[EQUIPO_AMERICA][Hospital]);
}

public DataEuropaLoad()
{
	new File[21] = "Datos de Europa.ini";
	Equipos[EQUIPO_EUROPA][Dinero] = dini_Int(File, "Dinero");
	Equipos[EQUIPO_EUROPA][Muerte] = dini_Int(File, "Muerte");
	Equipos[EQUIPO_EUROPA][Invasion] = dini_Int(File, "Invasion");
	Equipos[EQUIPO_EUROPA][Hospital] = dini_Int(File, "Hospital");
}

public DataEuropaSave()
{
    new File[21] = "Datos de Europa.ini";
    dini_IntSet(File, "Dinero", Equipos[EQUIPO_EUROPA][Dinero]);
    dini_IntSet(File, "Muerte", Equipos[EQUIPO_EUROPA][Muerte]);
    dini_IntSet(File, "Invasion", Equipos[EQUIPO_EUROPA][Invasion]);
    dini_IntSet(File, "Hospital", Equipos[EQUIPO_EUROPA][Hospital]);
}

public GivePlayerMoneyFromTeam(playerid, money, team)
{
	Equipos[team][Dinero] -= money;
	GivePlayerMoney(playerid, money);
}

public GetPlayerTeamEx(playerid)
{
	return PlayersDataOnline[playerid][Equipo];
}

public KickEx(playerid, razon[])
{
	new string[89];
	format(string, sizeof(string), "Has sido kickeado por:\n%s", razon);
	ShowPlayerDialog(playerid, 2, DIALOG_STYLE_MSGBOX, "Kickeado", string, "Aceptar", "");
	SetTimerEx("Kickear", 1000, false, "i", playerid);
}

public Kickear(playerid)
{
	Kick(playerid);
}

public BanEx2(playerid, razon[])
{
	new string[89];
	PlayersData[playerid][Baneado] = true;
	format(string, sizeof(string), "Has sido baneado por:\n%s", razon);
	ShowPlayerDialog(playerid, 3, DIALOG_STYLE_MSGBOX, "Baneado", string, "Aceptar", "");
	SetTimerEx("Banear", 1000, false, "is", playerid, razon);
}

public Banear(playerid, razon[])
{
	BanEx(playerid, razon);
}
